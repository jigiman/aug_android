package com.atlassian.augmeetup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.Console;

public class MainActivity extends AppCompatActivity {

    TextView tvResponse;
//    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvResponse = findViewById(R.id.txt_response);
//        progressBar = findViewById(R.id.progress_circular);

        getData(this);
    }

    private void getData(Context context){

        Ion.with(context)
                .load("http://165.22.223.35:6464/")
                .asJsonObject()
                .setCallback((e, result) -> {
//                    progressBar.setVisibility(View.GONE);
                    tvResponse.setVisibility(View.VISIBLE);
                    tvResponse.setText(result.get("payload").getAsString());
                    Log.d("response", result.get("payload").getAsString());
                });
    }

}
